#!/usr/bin/env perl

# based on live-http-headers.pl from Sniffer::HTTP module

use strict;
use warnings;
use utf8;

use Net::Pcap;
use Sniffer::HTTP;

$| = 1;
binmode STDOUT, ":utf8";

my $device = $ARGV[0];
my $filter = $ARGV[1];

if ($^O =~ /MSWin32|cygwin/ && $device) {
 $device = qr/$device/i
};

Sniffer::HTTP->new(
  callbacks => {
      response => sub { 
        my ($res,$req,$conn) = @_; 
	return unless defined $req;

	print "=== ".$conn->tcp_connection->src_host().":".$conn->tcp_connection->src_port()." --> ".
	  $conn->tcp_connection->dest_host().":".$conn->tcp_connection->dest_port()." ===\n";

	# === request ===
        print ">>>\n".$req->as_string."\n";

        if(defined($req->headers->header('Content-Type'))
            && ($req->headers->header('Content-Type') =~ m#^application/(json|x-www-form-urlencoded)#)) {
	  print $req->decoded_content."\n";
        }

	# === responce ===
        print "<<<\n".$res->status_line."\n".$res->headers->as_string."\n";

        if(defined($res->headers->header('Content-Type'))
            && ($res->headers->header('Content-Type') =~ m#^text/#)) {
	  print $res->decoded_content."\n";
        }
      }
  }
)->run($device, $filter);

