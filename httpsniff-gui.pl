#!/usr/bin/env perl

# HttpSniff
# (c) Alexander A Alexeev
# http://eax.me/

use strict;
use warnings;
use threads;
use threads::shared;

use Wx qw/:everything/;
use Wx::XRC;
use Thread::Queue;
use Net::Pcap;
use Sniffer::HTTP;
use Time::HiRes;
use JSON::XS;

use constant VERSION => '0.3.4';

our $terminate_sniffer_thread : shared = 0;
our $listen_interface : shared = undef;
our $filter_rules_version : shared = 1;
our $filter_rules : shared = encode_json({
    src_type => 'allow',
    dst_type => 'allow',
    src_list => [],
    dst_list => [],
  });

if($< != 0) {
  Wx::MessageBox('You need root privileges to use this porgram!', 'HttpSniff', Wx::wxICON_ERROR);
  exit;
} 

my $app = HttpSniffApp->new;
$app->MainLoop;

package AboutFrame;
use base qw/Wx::Frame/;
use Wx::Event qw/EVT_BUTTON/;

sub new {
  my $class = shift;
  my $app = shift;
  my $self = $class->SUPER::new;
  $self->{http_sniff_app} = $app;
  $self->initialize($app->{wx_xml_resource});
  return $self;
} 

sub initialize {
  my ($self, $xml_resource) = @_;
  $xml_resource->LoadFrame($self, undef, 'aboutFrame');
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_closeAboutButton'), \&closeAboutButtonOnClick);
}

sub closeAboutButtonOnClick {
  my $self = shift;
  $self->{http_sniff_app}{main_frame}->Enable(1);
  $self->Show(0);
}

package RuleFrame;
use base qw/Wx::Frame/;
use Wx::Event qw/EVT_BUTTON EVT_SET_FOCUS/;

sub new {
  my $class = shift;
  my $app = shift;
  my $self = $class->SUPER::new;
  $self->{http_sniff_app} = $app;
  $self->initialize($app->{wx_xml_resource});
  return $self;
} 

sub initialize {
  my ($self, $xml_resource) = @_;
  $xml_resource->LoadFrame($self, undef, 'ruleFrame');
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_ruleCancelButton'), \&ruleCancelButtonOnClick);
  EVT_SET_FOCUS($self->FindWindow('m_ruleHostText'), sub { $self->FindWindow('m_ruleCustomHostRadioBtn')->SetValue(1) });
  EVT_SET_FOCUS($self->FindWindow('m_rulePortText'), sub { $self->FindWindow('m_ruleCustomPortRadioBtn')->SetValue(1) });
}

sub ruleCancelButtonOnClick {
  my $self = shift;
  $self->{http_sniff_app}{main_frame}->Enable(1);
  $self->Show(0);
}

sub getHostPort {
  my ($self, $host, $port) = (shift, '*', '*');

  if($self->FindWindow('m_ruleCustomHostRadioBtn')->GetValue()) {
    $host = $self->FindWindow('m_ruleHostText')->GetValue();
  }
  if($self->FindWindow('m_ruleCustomPortRadioBtn')->GetValue()) {
    $port = $self->FindWindow('m_rulePortText')->GetValue();
  }
  return ($host, $port);
}
 
package MainFrame;
use base qw/Wx::Frame/;
use Wx::Event qw/EVT_CLOSE EVT_BUTTON EVT_LISTBOX EVT_MENU EVT_TIMER EVT_RADIOBOX/;

sub new {
  my $class = shift;
  my $app = shift;
  my $self = $class->SUPER::new;
  $self->{http_sniff_app} = $app;
  $self->initialize;
  return $self;
} 

sub initialize {
  my ($self) = @_;

  $self->{queries_info} = [];
  $self->{http_sniff_app}{wx_xml_resource}->LoadFrame($self, undef, 'mainFrame');

  my $timerId = 0;
  $self->{queue_timer} = Wx::Timer->new($self, $timerId);
  $self->{queue_timer}->Start(1000);

  EVT_LISTBOX($self, Wx::XmlResource::GetXRCID('m_queriesList'), \&queriesListOnClick);
  EVT_MENU($self, Wx::XmlResource::GetXRCID('m_menuExit'), \&menuExitOnClick);
  EVT_MENU($self, Wx::XmlResource::GetXRCID('m_menuClear'), \&menuClearOnClick);
  EVT_MENU($self, Wx::XmlResource::GetXRCID('m_menuAbout'), \&menuAboutOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_srcAddButton'), \&srcAddButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_srcEditButton'), \&srcEditButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_srcRemoveButton'), \&srcRemoveButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_dstAddButton'), \&dstAddButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_dstEditButton'), \&dstEditButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_dstRemoveButton'), \&dstRemoveButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_filtersCancelButton'), \&filterCancelButtonOnClick);
  EVT_BUTTON($self, Wx::XmlResource::GetXRCID('m_filtersApplyButton'), \&filterApplyButtonOnClick);
  EVT_LISTBOX($self, Wx::XmlResource::GetXRCID('m_interfacesListBox'), \&interfacesListBoxOnClick);
  EVT_RADIOBOX(
      $self, Wx::XmlResource::GetXRCID("m_${_}RadioBox"),
      sub { $self->FindWindow('m_filtersApplyButton')->Enable(1) }
    ) for (qw/src dst/);
  EVT_TIMER($self, $timerId, \&frameOnTimer);
  EVT_CLOSE($self, \&OnClose);

  my (%devinfo, $err);
  Net::Pcap::findalldevs(\%devinfo, \$err);
  die "Unable to enumerate devices: $err" if $err;

  my $list_box = $self->FindWindow('m_interfacesListBox');
  $list_box->Clear();
  for my $dev (sort keys %devinfo) {
    $list_box->Append("$dev --- $devinfo{$dev}");
  }
  $list_box->Select(0) if %devinfo;
  $self->interfacesListBoxOnClick();
}

sub OnClose {
  my $self = shift;
  $self->{queue_timer}->Stop();

  {
    lock($main::terminate_sniffer_thread);
    $main::terminate_sniffer_thread = 1;
  }

  for( $self->{http_sniff_app} ) {
    $_->{sniffer_thread}->join;
    $_->{about_frame}->Close();
    $_->{rule_frame}->Close();
  }

  $self->Destroy();
}

sub frameOnTimer {
  my $self = shift;
  my $list = $self->FindWindow('m_queriesList');
  while( my $info = $self->{http_sniff_app}{sniffer_queue}->dequeue_nb() ) {
    my ($sec, $min, $hour) = localtime($info->{time});
    my $time = sprintf "%02d:%02d:%02d", $hour, $min, $sec; 
    my ($query) = $info->{req} =~ /^([^\r\n]*)/;
    $query = substr($query, 0, 97)."..." if length($query) > 100;
    my $hostname = $info->{hostname} || 'undefined';
    $list->Append("$time [$hostname] $query ($info->{src_host}:$info->{src_port} --> $info->{dst_host}:$info->{dst_port})");
    push @{$self->{queries_info}}, $info;
  }
}

sub showRuleFrame {
  my ($self, $host, $port, $on_ok_click) = @_;
  $self->Enable(0);

  for my $rframe ( $self->{http_sniff_app}{rule_frame} ) {
    $rframe->FindWindow('m_ruleHostText')->SetValue($host eq '*' ? '' : $host);
    $rframe->FindWindow('m_ruleAnyHostRadioBtn')->SetValue($host eq '*');

    $rframe->FindWindow('m_rulePortText')->SetValue($port eq '*' ? '80' : $port);
    $rframe->FindWindow('m_ruleAnyPortRadioBtn')->SetValue($port eq '*');

    EVT_BUTTON($rframe, Wx::XmlResource::GetXRCID('m_ruleOkButton'), sub { 
        $rframe->Show(0);
        $on_ok_click->();
        $self->FindWindow('m_filtersApplyButton')->Enable(1);
        $self->Enable(1);
        $self->Raise();
      });
    $rframe->Center();
    $rframe->Show(1);
  }
}

sub addRule {
  my ($self, $list_box) = @_;
  my ($host, $port) = $self->{http_sniff_app}{rule_frame}->getHostPort();
  $list_box->Append("$host:$port");
}

sub editRule {
  my ($self, $list_box, $idx) = @_;
  my ($host, $port) = $self->{http_sniff_app}{rule_frame}->getHostPort();
  $list_box->SetString($idx, "$host:$port");
}

sub getListBoxIdxHostPort {
  my ($self, $list_box_name) = @_;
  my ($host, $port);

  my $list_box = $self->FindWindow($list_box_name);
  my $idx = $list_box->GetSelection();
  ($host, $port) = $list_box->GetString($idx) =~ /^(.*)\:(\d+|\*)$/
    if $idx >= 0;
  return ($list_box, $idx, $host, $port);
}

sub saveFilterRules {
  my ($self) = @_;
  my %filter_rules; 

  for my $sd (qw/src dst/) {
    $filter_rules{$sd.'_type'} = $self->FindWindow("m_${sd}RadioBox")->GetSelection() == 0 ? 'allow' : 'deny';
    $filter_rules{$sd.'_list'} = [];
    my @rules = $self->FindWindow("m_${sd}RulesListBox")->GetStrings();
    for my $rule(@rules) {
      my ($host, $port) = $rule =~ /^(.*)\:(\d+|\*)$/;
      push @{$filter_rules{$sd.'_list'}}, { host => $host, port => $port };
    }
  }

  return \%filter_rules;
}

sub loadFilterRules {
  my ($self, $filter_rules) = @_;

  for my $sd (qw/src dst/) {
    $self->FindWindow("m_${sd}RadioBox")->SetSelection($filter_rules->{$sd.'_type'} eq 'deny');

    my $list_box = $self->FindWindow("m_${sd}RulesListBox");
    $list_box->Clear();

    for my $rule( @{$filter_rules->{$sd.'_list'}} ) {
      $list_box->Append($rule->{host}.':'.$rule->{port});
    }
  }
}

sub filterApplyButtonOnClick {
  my $self = shift;
  my $rules = $self->saveFilterRules();
  {
    lock($main::filter_rules_version);
    $main::filter_rules = JSON::XS::encode_json($rules);
    $main::filter_rules_version++;
  }
  $self->FindWindow('m_filtersApplyButton')->Enable(0);
}

sub interfacesListBoxOnClick {
  my $self = shift;
  my $list_box = $self->FindWindow('m_interfacesListBox');
  my $interface = $list_box->GetStringSelection();
  $interface =~ /^(.*?) ---/;
  $interface = $1;
  {
    lock($main::listen_interface);
    $main::listen_interface = $interface;
  }
  if($self->{http_sniff_app}{sniffer_thread}) {
    {
      lock($main::terminate_sniffer_thread);
      $main::terminate_sniffer_thread = 1;
    }
    for ($self->{http_sniff_app} ) {
      $_->{sniffer_thread}->join;

      {
        lock($main::terminate_sniffer_thread);
        $main::terminate_sniffer_thread = 0;
      }

      $_->{sniffer_thread} = threads->create(\&HttpSniffApp::snifferProc, $_->{sniffer_queue});
    }
  }
}

sub filterCancelButtonOnClick {
  my $self = shift;
  my $rules;
  {
    lock($main::filter_rules_version);
    $rules = JSON::XS::decode_json($main::filter_rules);
  }
  $self->loadFilterRules($rules);
  $self->FindWindow('m_filtersApplyButton')->Enable(0);
}

sub srcAddButtonOnClick {
  my $self = shift;
  $self->showRuleFrame('*', '*', sub { $self->addRule($self->FindWindow('m_srcRulesListBox')) });
}

sub srcEditButtonOnClick {
  my $self = shift;
  my ($list_box, $idx, $host, $port) = $self->getListBoxIdxHostPort('m_srcRulesListBox');
  return unless $idx >= 0;
  $self->showRuleFrame($host, $port, sub { $self->editRule($list_box, $idx) });
}

sub srcRemoveButtonOnClick {
  my $self = shift;
  my ($list_box, $idx) = $self->getListBoxIdxHostPort('m_srcRulesListBox');
  return unless $idx >= 0;
  $list_box->Delete($idx);
  $self->FindWindow('m_filtersApplyButton')->Enable(1);
}

sub dstAddButtonOnClick {
  my $self = shift;
  $self->showRuleFrame('*', '*', sub { $self->addRule($self->FindWindow('m_dstRulesListBox')) });
}

sub dstEditButtonOnClick {
  my $self = shift;
  my ($list_box, $idx, $host, $port) = $self->getListBoxIdxHostPort('m_dstRulesListBox');
  return unless $idx >= 0;
  $self->showRuleFrame($host, $port, sub { $self->editRule($list_box, $idx) });
}

sub dstRemoveButtonOnClick {
  my $self = shift;
  my ($list_box, $idx) = $self->getListBoxIdxHostPort('m_dstRulesListBox');
  return unless $idx >= 0;
  $list_box->Delete($idx);
  $self->FindWindow('m_filtersApplyButton')->Enable(1);
}

sub menuExitOnClick {
  my $self = shift;
  $self->Close();
}

sub menuClearOnClick {
  my $self = shift;
  $self->{queries_info} = [];
  $self->FindWindow('m_queriesList')->Set([]);
}

sub menuAboutOnClick {
  my $self = shift;
  $self->{http_sniff_app}{main_frame}->Enable(0);
  for( $self->{http_sniff_app}{about_frame} ) {
    $_->Center();
    $_->Show(1);
    $_->Raise();
  }
}

sub queriesListOnClick {
  my $self = shift;
  my $i = $self->FindWindow('m_queriesList')->GetSelection;

  my ($request, $response) = ('', '');

  if($i >= 0 && $i < scalar(@{$self->{queries_info}})) {
    my $info = $self->{queries_info}->[$i];
    $request = $info->{req};
    $request .= "\n$info->{req_content}" if defined $info->{req_content};

    $response = $info->{res};
    $response .= "\n$info->{res_content}" if defined $info->{res_content};
  }

  $self->FindWindow('m_requestText')->SetValue($request);
  $self->FindWindow('m_responseText')->SetValue($response);
}

package HttpSniffApp;
use base qw/Wx::App/;

sub OnInit {
  my $self = shift;
  $self->{sniffer_queue} = Thread::Queue->new();

  $self->{wx_xml_resource} = Wx::XmlResource->new;
  for( $self->{wx_xml_resource} ) {
    $_->InitAllHandlers;
    $_->Load('httpsniff.xrc');
  }

  $self->{main_frame} = MainFrame->new($self, -1);

  for( $self->{main_frame} ) {
    $self->SetTopWindow( $_ );
    $_->SetTitle('HttpSniff v'.main::VERSION);
    $_->Show(1);
    $_->Maximize(1);

    for my $splitter (qw/m_horizontalSplitter m_verticalSplitter/) {
      $_->FindWindow($splitter)->SetSashGravity(0.5);
    }
  }

  $self->{about_frame} = AboutFrame->new($self, -1);
  $self->{rule_frame} = RuleFrame->new($self, -1);
  $self->{rule_frame}->Fit();

  for( $self->{about_frame} ) {
    $_->FindWindow('m_progNameText')->SetLabel('HttpSniff v'.main::VERSION);
    $_->Fit();
  }

  $self->{sniffer_thread} = threads->create(\&snifferProc, $self->{sniffer_queue});

  return 1;
}

sub snifferProc {
  my $queue = shift;
  my $filter_rules_version = 0;
  my $filter_rules;

  my $sniffer = Sniffer::HTTP->new(
    callbacks => {
      response => sub { 
        my @args = @_; 
        push @args, $queue;
        push @args, $filter_rules;
        return snifferOnResponse(@args);
      }
    }
  );

  my $pcap = eval { createPcap() };
  if($@) {
    warn "createPcap: $@";
    return 2;
  }

  my $term = 0;
  while(!$term) {
    my $was_processed = 0;
    {
      lock($main::filter_rules_version);
      if($main::filter_rules_version > $filter_rules_version) {
        $filter_rules = JSON::XS::decode_json($main::filter_rules);
        $filter_rules_version = $main::filter_rules_version;
      }
    }

    eval {
      Net::Pcap::pcap_dispatch($pcap, 100, sub {
          eval {
            $sniffer->handle_eth_packet($_[2], $_[1]->{tv_sec});
          };
          warn "handle_eth_packet: $@" if $@; 
          $was_processed++;
        },'');
    };
    warn "pcap_dispatch: $@" if $@;

    {
      lock($main::terminate_sniffer_thread);
      $term = $main::terminate_sniffer_thread;
    }

    Time::HiRes::usleep(100) unless $was_processed && !$term; 
  }
  Net::Pcap::pcap_close($pcap);
  return 1;
}

sub hostAndPortMatch {
  my ($host, $port, $hp_list) = @_;
  $host .= '$';
  for my $hp(@{$hp_list}) {
    my $host_match = ($hp->{host} eq '*') || (index($host, $hp->{host}) == 0);
    my $port_match = ($hp->{port} eq '*') || ($port == $hp->{port});
    return 1 if $host_match && $port_match;
  }
  return 0;
}

sub shouldBeFiltered {
  my ($q_info, $filter_rules) = @_;

  my $src_match = hostAndPortMatch($q_info->{src_host}, $q_info->{src_port}, $filter_rules->{src_list});
  return 1 if ($filter_rules->{src_type} eq 'allow') == $src_match;

  my $dst_match = hostAndPortMatch($q_info->{dst_host}, $q_info->{dst_port}, $filter_rules->{dst_list});
  return 1 if ($filter_rules->{dst_type} eq 'allow') == $dst_match;

  return 0;
}

sub snifferOnResponse {
  my ($res,$req,$conn, $queue, $filter_rules) = @_; 
  return unless defined $req;

  my $tcp_conn = $conn->tcp_connection;

  my $http_req = $req->as_string;
  $http_req =~ s#^(\S+ )https?://([^/]+)#$1#;
  my $hostname = $2;

  my $info = {
       time => time(),
       hostname => $hostname,
       src_host => $tcp_conn->src_host(),
       src_port => $tcp_conn->src_port(),
       dst_host => $tcp_conn->dest_host(),
       dst_port => $tcp_conn->dest_port(),
       req => $http_req,
       res => $res->status_line."\n".$res->headers->as_string,
     };

  return if shouldBeFiltered($info, $filter_rules);

  if(defined($res->headers->header('Content-Type'))
      && ($res->headers->header('Content-Type') =~ m#^(text/|application/json)#)) {
    $info->{res_content} = $res->decoded_content;
  }

  $queue->enqueue($info);
}

sub createPcap {
  my $device = undef;
  while(!$device) {
    Time::HiRes::usleep(100);
    {
      lock($main::listen_interface);
      $device = $main::listen_interface;
    }
  }

  my ($netmask, $address, $err);
  if(Net::Pcap::lookupnet($device, \$address, \$netmask, \$err)) {
    die "Unable to look up device information for '$device': $err";
  }

  my $pcap = Net::Pcap::open_live($device, 16384, -1, 300, \$err);
  unless (defined $pcap) {
    die "Unable to create packet capture on device '$device': $err";
  };

  Net::Pcap::pcap_setnonblock($pcap, 0, \$err);

  my $filter;
  Net::Pcap::pcap_compile($pcap, \$filter, "tcp", 0, 0);
  Net::Pcap::pcap_setfilter($pcap, $filter);
  Net::Pcap::pcap_freecode($filter);

  return $pcap;
}

